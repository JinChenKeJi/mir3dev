/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50538
Source Host           : localhost:3306
Source Database       : legendofmir

Target Server Type    : MYSQL
Target Server Version : 50538
File Encoding         : 936

Date: 2016-12-06 01:27:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `dtproperties`
-- ----------------------------
DROP TABLE IF EXISTS `dtproperties`;
CREATE TABLE `dtproperties` (
  `id` int(11) NOT NULL,
  `objectid` int(11) DEFAULT NULL,
  `property` varchar(64) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `uvalue` varchar(255) DEFAULT NULL,
  `lvalue` longblob,
  `version` int(11) NOT NULL,
  PRIMARY KEY (`id`,`property`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dtproperties
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_account`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_account`;
CREATE TABLE `tbl_account` (
  `FLD_LOGINID` char(10) NOT NULL,
  `FLD_PASSWORD` char(10) NOT NULL,
  `FLD_USERNAME` char(20) NOT NULL,
  `FLD_CERTIFICATION` int(11) DEFAULT NULL,
  PRIMARY KEY (`FLD_LOGINID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_account
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_accountadd`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_accountadd`;
CREATE TABLE `tbl_accountadd` (
  `FLD_LOGINID` char(10) NOT NULL,
  `FLD_SSNO` char(14) NOT NULL,
  `FLD_BIRTHDAY` char(10) NOT NULL,
  `FLD_PHONE` char(14) NOT NULL,
  `FLD_MOBILEPHONE` char(13) NOT NULL,
  `FLD_ADDRESS1` varchar(20) DEFAULT NULL,
  `FLD_ADDRESS2` varchar(20) DEFAULT NULL,
  `FLD_EMAIL` varchar(40) DEFAULT NULL,
  `FLD_QUIZ1` varchar(20) DEFAULT NULL,
  `FLD_ANSWER1` varchar(20) DEFAULT NULL,
  `FLD_QUIZ2` varchar(20) DEFAULT NULL,
  `FLD_ANSWER2` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`FLD_LOGINID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_accountadd
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_character`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_character`;
CREATE TABLE `tbl_character` (
  `FLD_LOGINID` char(10) NOT NULL,
  `FLD_CHARNAME` char(20) NOT NULL,
  `FLD_JOB` char(1) NOT NULL,
  `FLD_GENDER` char(1) NOT NULL,
  `FLD_LEVEL` tinyint(4) NOT NULL,
  `FLD_DIRECTION` int(11) NOT NULL,
  `FLD_CX` int(11) NOT NULL,
  `FLD_CY` int(11) NOT NULL,
  `FLD_MAPNAME` char(16) NOT NULL,
  `FLD_GOLD` int(11) NOT NULL,
  `FLD_DRESS_ID` char(11) NOT NULL,
  `FLD_WEAPON_ID` char(11) NOT NULL,
  `FLD_LEFTHAND_ID` char(11) NOT NULL,
  `FLD_RIGHTHAND_ID` char(11) NOT NULL,
  `FLD_HELMET_ID` char(11) NOT NULL,
  `FLD_NECKLACE_ID` char(11) NOT NULL,
  `FLD_ARMRINGL_ID` char(11) NOT NULL,
  `FLD_ARMRINGR_ID` char(11) NOT NULL,
  `FLD_RINGL_ID` char(11) NOT NULL,
  `FLD_RINGR_ID` char(11) NOT NULL,
  PRIMARY KEY (`FLD_LOGINID`,`FLD_CHARNAME`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_character
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_character_item`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_character_item`;
CREATE TABLE `tbl_character_item` (
  `FLD_LOGINID` char(10) NOT NULL,
  `FLD_CHARNAME` char(20) NOT NULL,
  `FLD_ITEM1` char(12) DEFAULT NULL,
  `FLD_ITEM2` char(12) DEFAULT NULL,
  `FLD_ITEM3` char(12) DEFAULT NULL,
  `FLD_ITEM4` char(12) DEFAULT NULL,
  `FLD_ITEM5` char(12) DEFAULT NULL,
  `FLD_ITEM6` char(12) DEFAULT NULL,
  `FLD_ITEM7` char(12) DEFAULT NULL,
  `FLD_ITEM8` char(12) DEFAULT NULL,
  `FLD_ITEM9` char(12) DEFAULT NULL,
  `FLD_ITEM10` char(12) DEFAULT NULL,
  `FLD_ITEM11` char(12) DEFAULT NULL,
  `FLD_ITEM12` char(12) DEFAULT NULL,
  `FLD_ITEM13` char(12) DEFAULT NULL,
  `FLD_ITEM14` char(12) DEFAULT NULL,
  `FLD_ITEM15` char(12) DEFAULT NULL,
  `FLD_ITEM16` char(12) DEFAULT NULL,
  `FLD_ITEM17` char(12) DEFAULT NULL,
  `FLD_ITEM18` char(12) DEFAULT NULL,
  `FLD_ITEM19` char(12) DEFAULT NULL,
  `FLD_ITEM20` char(12) DEFAULT NULL,
  `FLD_ITEM21` char(12) DEFAULT NULL,
  `FLD_ITEM22` char(12) DEFAULT NULL,
  `FLD_ITEM23` char(12) DEFAULT NULL,
  `FLD_ITEM24` char(12) DEFAULT NULL,
  `FLD_ITEM25` char(12) DEFAULT NULL,
  `FLD_ITEM26` char(12) DEFAULT NULL,
  `FLD_ITEM27` char(12) DEFAULT NULL,
  `FLD_ITEM28` char(12) DEFAULT NULL,
  `FLD_ITEM29` char(12) DEFAULT NULL,
  `FLD_ITEM30` char(12) DEFAULT NULL,
  `FLD_ITEM31` char(12) DEFAULT NULL,
  `FLD_ITEM32` char(12) DEFAULT NULL,
  `FLD_ITEM33` char(12) DEFAULT NULL,
  `FLD_ITEM34` char(12) DEFAULT NULL,
  `FLD_ITEM35` char(12) DEFAULT NULL,
  `FLD_ITEM36` char(12) DEFAULT NULL,
  `FLD_ITEM37` char(12) DEFAULT NULL,
  `FLD_ITEM38` char(12) DEFAULT NULL,
  `FLD_ITEM39` char(12) DEFAULT NULL,
  `FLD_ITEM40` char(12) DEFAULT NULL,
  `FLD_ITEM41` char(12) DEFAULT NULL,
  `FLD_ITEM42` char(12) DEFAULT NULL,
  `FLD_ITEM43` char(12) DEFAULT NULL,
  `FLD_ITEM44` char(12) DEFAULT NULL,
  `FLD_ITEM45` char(12) DEFAULT NULL,
  `FLD_ITEM46` char(12) DEFAULT NULL,
  `FLD_ITEM47` char(12) DEFAULT NULL,
  `FLD_ITEM48` char(12) DEFAULT NULL,
  `FLD_ITEM49` char(12) DEFAULT NULL,
  `FLD_ITEM50` char(12) DEFAULT NULL,
  `FLD_ITEM51` char(12) DEFAULT NULL,
  `FLD_ITEM52` char(12) DEFAULT NULL,
  `FLD_ITEM53` char(12) DEFAULT NULL,
  `FLD_ITEM54` char(12) DEFAULT NULL,
  `FLD_ITEM55` char(12) DEFAULT NULL,
  `FLD_ITEM56` char(12) DEFAULT NULL,
  `FLD_ITEM57` char(12) DEFAULT NULL,
  `FLD_ITEM58` char(12) DEFAULT NULL,
  `FLD_ITEM59` char(12) DEFAULT NULL,
  `FLD_ITEM60` char(12) DEFAULT NULL,
  `FLD_ITEM61` char(12) DEFAULT NULL,
  `FLD_ITEM62` char(12) DEFAULT NULL,
  `FLD_ITEM63` char(12) DEFAULT NULL,
  `FLD_ITEM64` char(12) DEFAULT NULL,
  `FLD_ITEM65` char(12) DEFAULT NULL,
  `FLD_ITEM66` char(12) DEFAULT NULL,
  PRIMARY KEY (`FLD_LOGINID`,`FLD_CHARNAME`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_character_item
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_character_magic`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_character_magic`;
CREATE TABLE `tbl_character_magic` (
  `FLD_LOGINID` char(10) NOT NULL,
  `FLD_CHARNAME` char(20) NOT NULL,
  `FLD_LEVEL0` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY0` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN0` int(11) DEFAULT NULL,
  `FLD_LEVEL1` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY1` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN1` int(11) DEFAULT NULL,
  `FLD_LEVEL2` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY2` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN2` int(11) DEFAULT NULL,
  `FLD_LEVEL3` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY3` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN3` int(11) DEFAULT NULL,
  `FLD_LEVEL4` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY4` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN4` int(11) DEFAULT NULL,
  `FLD_LEVEL5` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY5` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN5` int(11) DEFAULT NULL,
  `FLD_LEVEL6` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY6` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN6` int(11) DEFAULT NULL,
  `FLD_LEVEL7` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY7` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN7` int(11) DEFAULT NULL,
  `FLD_LEVEL8` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY8` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN8` int(11) DEFAULT NULL,
  `FLD_LEVEL9` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY9` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN9` int(11) DEFAULT NULL,
  `FLD_LEVEL10` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY10` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN10` int(11) DEFAULT NULL,
  `FLD_LEVEL11` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY11` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN11` int(11) DEFAULT NULL,
  `FLD_LEVEL12` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY12` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN12` int(11) DEFAULT NULL,
  `FLD_LEVEL13` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY13` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN13` int(11) DEFAULT NULL,
  `FLD_LEVEL14` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY14` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN14` int(11) DEFAULT NULL,
  `FLD_LEVEL15` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY15` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN15` int(11) DEFAULT NULL,
  `FLD_LEVEL16` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY16` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN16` int(11) DEFAULT NULL,
  `FLD_LEVEL17` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY17` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN17` int(11) DEFAULT NULL,
  `FLD_LEVEL18` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY18` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN18` int(11) DEFAULT NULL,
  `FLD_LEVEL19` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY19` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN19` int(11) DEFAULT NULL,
  `FLD_LEVEL20` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY20` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN20` int(11) DEFAULT NULL,
  `FLD_LEVEL21` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY21` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN21` int(11) DEFAULT NULL,
  `FLD_LEVEL22` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY22` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN22` int(11) DEFAULT NULL,
  `FLD_LEVEL23` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY23` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN23` int(11) DEFAULT NULL,
  `FLD_LEVEL24` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY24` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN24` int(11) DEFAULT NULL,
  `FLD_LEVEL25` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY25` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN25` int(11) DEFAULT NULL,
  `FLD_LEVEL26` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY26` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN26` int(11) DEFAULT NULL,
  `FLD_LEVEL27` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY27` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN27` int(11) DEFAULT NULL,
  `FLD_LEVEL28` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY28` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN28` int(11) DEFAULT NULL,
  `FLD_LEVEL29` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY29` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN29` int(11) DEFAULT NULL,
  `FLD_LEVEL30` tinyint(4) DEFAULT NULL,
  `FLD_USEKEY30` tinyint(4) DEFAULT NULL,
  `FLD_CURRTRAIN30` int(11) DEFAULT NULL,
  PRIMARY KEY (`FLD_LOGINID`,`FLD_CHARNAME`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_character_magic
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_guard`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_guard`;
CREATE TABLE `tbl_guard` (
  `FLD_INDEX` int(11) NOT NULL,
  `FLD_ID` char(14) NOT NULL,
  `FLD_MAPNAME` char(14) NOT NULL,
  `FLD_POSX` int(11) NOT NULL,
  `FLD_POSY` int(11) NOT NULL,
  `FLD_DIRECTION` smallint(6) NOT NULL,
  `FLD_DESCRIPTION` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_guard
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_horse`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_horse`;
CREATE TABLE `tbl_horse` (
  `FLD_CHARNAME` char(20) NOT NULL,
  `FLD_HORSEINDEX` char(11) NOT NULL,
  `FLD_HORSETYPE` tinyint(4) NOT NULL,
  `FLD_LEVEL` tinyint(4) DEFAULT NULL,
  `FLD_PRICE` int(11) DEFAULT NULL,
  `FLD_HP` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`FLD_CHARNAME`,`FLD_HORSEINDEX`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_horse
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_magic`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_magic`;
CREATE TABLE `tbl_magic` (
  `FLD_INDEX` int(11) NOT NULL,
  `FLD_NAME` char(12) NOT NULL,
  `FLD_EFFECTTYPE` smallint(6) DEFAULT NULL,
  `FLD_EFFECT` smallint(6) DEFAULT NULL,
  `FLD_SPELL` smallint(6) DEFAULT NULL,
  `FLD_POWER` smallint(6) DEFAULT NULL,
  `FLD_MAXPOWER` smallint(6) DEFAULT NULL,
  `FLD_DEFSPELL` smallint(6) DEFAULT NULL,
  `FLD_DEFPOWER` smallint(6) DEFAULT NULL,
  `FLD_DEFMAXPOWER` smallint(6) DEFAULT NULL,
  `FLD_JOB` smallint(6) DEFAULT NULL,
  `FLD_NEEDL1` smallint(6) DEFAULT NULL,
  `FLD_L1TRAIN` int(11) DEFAULT NULL,
  `FLD_NEEDL2` smallint(6) DEFAULT NULL,
  `FLD_L2TRAIN` int(11) DEFAULT NULL,
  `FLD_NEEDL3` smallint(6) DEFAULT NULL,
  `FLD_L3TRAIN` int(11) DEFAULT NULL,
  `FLD_DELAY` smallint(6) DEFAULT NULL,
  `FLD_DESC` char(8) DEFAULT NULL,
  `FLD_DESCRIPTION` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`FLD_INDEX`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_magic
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_mapinfo`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_mapinfo`;
CREATE TABLE `tbl_mapinfo` (
  `FLD_MAPFILENAME` char(14) NOT NULL,
  `FLD_MAPNAME` varchar(40) NOT NULL,
  `FLD_SERVERINDEX` smallint(6) NOT NULL,
  `FLD_ATTRIBUTE` int(11) DEFAULT NULL,
  `FLD_RECALLMAPFNAME` char(14) DEFAULT NULL,
  `FLD_DESCRIPTION` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`FLD_MAPFILENAME`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_mapinfo
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_merchant`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_merchant`;
CREATE TABLE `tbl_merchant` (
  `FLD_ID` int(11) NOT NULL,
  `FLD_MAPNAME` char(14) NOT NULL,
  `FLD_POSX` int(11) NOT NULL,
  `FLD_POSY` int(11) NOT NULL,
  `FLD_NPCNAME` varchar(40) NOT NULL,
  `FLD_FACE` int(11) NOT NULL,
  `FLD_BODY` int(11) NOT NULL,
  `FLD_GENDER` smallint(6) NOT NULL,
  `FLD_DESCRIPTION` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`FLD_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_merchant
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_mongen`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_mongen`;
CREATE TABLE `tbl_mongen` (
  `FLD_INDEX` int(11) NOT NULL,
  `FLD_MAPNAME` char(14) NOT NULL,
  `FLD_X` int(11) NOT NULL,
  `FLD_Y` int(11) DEFAULT NULL,
  `FLD_MONNAME` char(14) DEFAULT NULL,
  `FLD_AREA` smallint(6) DEFAULT NULL,
  `FLD_COUNT` smallint(6) DEFAULT NULL,
  `FLD_GENTIME` int(11) DEFAULT NULL,
  `FLD_SMALLGENRATE` int(11) DEFAULT NULL,
  `FLD_DESCRIPTION` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`FLD_INDEX`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_mongen
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_monster`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_monster`;
CREATE TABLE `tbl_monster` (
  `FLD_NAME` char(14) NOT NULL,
  `FLD_RACE` smallint(6) DEFAULT NULL,
  `FLD_RACEIMG` smallint(6) DEFAULT NULL,
  `FLD_APPR` smallint(6) DEFAULT NULL,
  `FLD_LEVEL` smallint(6) DEFAULT NULL,
  `FLD_UNDEAD` smallint(6) DEFAULT NULL,
  `FLD_EXP` smallint(6) DEFAULT NULL,
  `FLD_HP` smallint(6) DEFAULT NULL,
  `FLD_MP` smallint(6) DEFAULT NULL,
  `FLD_AC` smallint(6) DEFAULT NULL,
  `FLD_MAXAC` smallint(6) DEFAULT NULL,
  `FLD_MAC` smallint(6) DEFAULT NULL,
  `FLD_MAXMAC` smallint(6) DEFAULT NULL,
  `FLD_DC` smallint(6) DEFAULT NULL,
  `FLD_MAXDC` smallint(6) DEFAULT NULL,
  `FLD_SPEED` smallint(6) DEFAULT NULL,
  `FLD_HIT` smallint(6) DEFAULT NULL,
  `FLD_WALKSPEED` smallint(6) DEFAULT NULL,
  `FLD_ATTACKSPEED` smallint(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_monster
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_movemapevent`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_movemapevent`;
CREATE TABLE `tbl_movemapevent` (
  `FLD_INDEX` int(11) NOT NULL,
  `FLD_SMAPFILENAME` char(14) NOT NULL,
  `FLD_SX` int(11) NOT NULL,
  `FLD_SY` int(11) NOT NULL,
  `FLD_DMAPFILENAME` char(14) NOT NULL,
  `FLD_DX` int(11) NOT NULL,
  `FLD_DY` int(11) NOT NULL,
  `FLD_DESCRIPTION` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`FLD_INDEX`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_movemapevent
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_npc`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_npc`;
CREATE TABLE `tbl_npc` (
  `FLD_INDEX` int(11) NOT NULL,
  `FLD_NAME` char(14) NOT NULL,
  `FLD_RACE` smallint(6) NOT NULL,
  `FLD_MAPNAME` char(14) NOT NULL,
  `FLD_POSX` int(11) NOT NULL,
  `FLD_POSY` int(11) NOT NULL,
  `FLD_FACE` smallint(6) NOT NULL,
  `FLD_BODY` smallint(6) NOT NULL,
  `FLD_DESCRIPTION` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_npc
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_serverinfo`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_serverinfo`;
CREATE TABLE `tbl_serverinfo` (
  `FLD_SERVERIDX` int(11) NOT NULL,
  `FLD_SERVERNAME` char(40) NOT NULL,
  `FLD_SERVERIP` char(20) NOT NULL,
  PRIMARY KEY (`FLD_SERVERIDX`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_serverinfo
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_startpoint`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_startpoint`;
CREATE TABLE `tbl_startpoint` (
  `FLD_INDEX` int(11) NOT NULL,
  `FLD_MAPNAME` char(14) NOT NULL,
  `FLD_POSX` int(11) NOT NULL,
  `FLD_POSY` int(11) NOT NULL,
  `FLD_DESCRIPTION` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_startpoint
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_stditem`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_stditem`;
CREATE TABLE `tbl_stditem` (
  `FLD_INDEX` int(11) NOT NULL,
  `FLD_NAME` char(14) NOT NULL,
  `FLD_STDMODE` smallint(6) DEFAULT NULL,
  `FLD_SHAPE` smallint(6) DEFAULT NULL,
  `FLD_WEIGHT` smallint(6) DEFAULT NULL,
  `FLD_ANICOUNT` smallint(6) DEFAULT NULL,
  `FLD_SOURCE` smallint(6) DEFAULT NULL,
  `FLD_RESERVED` smallint(6) DEFAULT NULL,
  `FLD_LOOKS` int(11) DEFAULT NULL,
  `FLD_DURAMAX` int(11) DEFAULT NULL,
  `FLD_AC` smallint(6) DEFAULT NULL,
  `FLD_AC2` smallint(6) DEFAULT NULL,
  `FLD_MAC` smallint(6) DEFAULT NULL,
  `FLD_MAC2` smallint(6) DEFAULT NULL,
  `FLD_DC` smallint(6) DEFAULT NULL,
  `FLD_DC2` smallint(6) DEFAULT NULL,
  `FLD_MC` smallint(6) DEFAULT NULL,
  `FLD_MC2` smallint(6) DEFAULT NULL,
  `FLD_SC` smallint(6) DEFAULT NULL,
  `FLD_SC2` smallint(6) DEFAULT NULL,
  `FLD_NEED` smallint(6) DEFAULT NULL,
  `FLD_NEEDLEVEL` smallint(6) DEFAULT NULL,
  `FLD_PRICE` int(11) DEFAULT NULL,
  `FLD_STOCK` int(11) DEFAULT NULL,
  `FLD_DESCRIPTION` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`FLD_INDEX`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_stditem
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_stditemarmor`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_stditemarmor`;
CREATE TABLE `tbl_stditemarmor` (
  `FLD_INDEX` int(11) NOT NULL,
  `FLD_NAME` varchar(20) NOT NULL,
  `FLD_STDMODE` smallint(6) DEFAULT NULL,
  `FLD_SHAPE` smallint(6) DEFAULT NULL,
  `FLD_WEIGHT` smallint(6) DEFAULT NULL,
  `FLD_ANICOUNT` smallint(6) DEFAULT NULL,
  `FLD_SOURCE` smallint(6) DEFAULT NULL,
  `FLD_LOOKS` int(11) DEFAULT NULL,
  `FLD_DURAMAX` int(11) DEFAULT NULL,
  `FLD_AC` smallint(6) DEFAULT NULL,
  `FLD_AC2` smallint(6) DEFAULT NULL,
  `FLD_MAC` smallint(6) DEFAULT NULL,
  `FLD_MAC2` smallint(6) DEFAULT NULL,
  `FLD_DC` smallint(6) DEFAULT NULL,
  `FLD_DC2` smallint(6) DEFAULT NULL,
  `FLD_MC` smallint(6) DEFAULT NULL,
  `FLD_MC2` smallint(6) DEFAULT NULL,
  `FLD_SC` smallint(6) DEFAULT NULL,
  `FLD_SC2` smallint(6) DEFAULT NULL,
  `FLD_NEED` smallint(6) DEFAULT NULL,
  `FLD_NEEDLEVEL` smallint(6) DEFAULT NULL,
  `FLD_PRICE` int(11) DEFAULT NULL,
  `FLD_STOCK` int(11) DEFAULT NULL,
  PRIMARY KEY (`FLD_INDEX`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_stditemarmor
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_stditemweapon`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_stditemweapon`;
CREATE TABLE `tbl_stditemweapon` (
  `FLD_INDEX` int(11) NOT NULL,
  `FLD_NAME` varchar(20) NOT NULL,
  `FLD_STDMODE` smallint(6) DEFAULT NULL,
  `FLD_SHAPE` smallint(6) DEFAULT NULL,
  `FLD_WEIGHT` smallint(6) DEFAULT NULL,
  `FLD_ANICOUNT` smallint(6) DEFAULT NULL,
  `FLD_SOURCE` smallint(6) DEFAULT NULL,
  `FLD_LOOKS` int(11) DEFAULT NULL,
  `FLD_DURAMAX` int(11) DEFAULT NULL,
  `FLD_AC` smallint(6) DEFAULT NULL,
  `FLD_AC2` smallint(6) DEFAULT NULL,
  `FLD_MAC` smallint(6) DEFAULT NULL,
  `FLD_MAC2` smallint(6) DEFAULT NULL,
  `FLD_DC` smallint(6) DEFAULT NULL,
  `FLD_DC2` smallint(6) DEFAULT NULL,
  `FLD_MC` smallint(6) DEFAULT NULL,
  `FLD_MC2` smallint(6) DEFAULT NULL,
  `FLD_SC` smallint(6) DEFAULT NULL,
  `FLD_SC2` smallint(6) DEFAULT NULL,
  `FLD_NEED` smallint(6) DEFAULT NULL,
  `FLD_NEEDLEVEL` smallint(6) DEFAULT NULL,
  `FLD_PRICE` int(11) DEFAULT NULL,
  `FLD_STOCK` int(11) DEFAULT NULL,
  PRIMARY KEY (`FLD_INDEX`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_stditemweapon
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_stditem_accessory`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_stditem_accessory`;
CREATE TABLE `tbl_stditem_accessory` (
  `FLD_INDEX` int(11) NOT NULL,
  `FLD_NAME` varchar(20) NOT NULL,
  `FLD_STDMODE` smallint(6) DEFAULT NULL,
  `FLD_SHAPE` smallint(6) DEFAULT NULL,
  `FLD_WEIGHT` smallint(6) DEFAULT NULL,
  `FLD_ANICOUNT` smallint(6) DEFAULT NULL,
  `FLD_SOURCE` smallint(6) DEFAULT NULL,
  `FLD_LOOKS` int(11) DEFAULT NULL,
  `FLD_DURAMAX` int(11) DEFAULT NULL,
  `FLD_AC` smallint(6) DEFAULT NULL,
  `FLD_AC2` smallint(6) DEFAULT NULL,
  `FLD_MAC` smallint(6) DEFAULT NULL,
  `FLD_MAC2` smallint(6) DEFAULT NULL,
  `FLD_DC` smallint(6) DEFAULT NULL,
  `FLD_DC2` smallint(6) DEFAULT NULL,
  `FLD_MC` smallint(6) DEFAULT NULL,
  `FLD_MC2` smallint(6) DEFAULT NULL,
  `FLD_SC` smallint(6) DEFAULT NULL,
  `FLD_SC2` smallint(6) DEFAULT NULL,
  `FLD_NEED` smallint(6) DEFAULT NULL,
  `FLD_NEEDLEVEL` smallint(6) DEFAULT NULL,
  `FLD_PRICE` int(11) DEFAULT NULL,
  `FLD_STOCK` int(11) DEFAULT NULL,
  PRIMARY KEY (`FLD_INDEX`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_stditem_accessory
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_stditem_etc`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_stditem_etc`;
CREATE TABLE `tbl_stditem_etc` (
  `FLD_INDEX` int(11) NOT NULL,
  `FLD_NAME` varchar(20) NOT NULL,
  `FLD_STDMODE` smallint(6) DEFAULT NULL,
  `FLD_SHAPE` smallint(6) DEFAULT NULL,
  `FLD_LOOKS` int(11) DEFAULT NULL,
  `FLD_WEIGHT` smallint(6) DEFAULT NULL,
  `FLD_PRICE` int(11) DEFAULT NULL,
  `FLD_VAL1` int(11) DEFAULT NULL,
  `FLD_VAL2` int(11) DEFAULT NULL,
  PRIMARY KEY (`FLD_INDEX`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_stditem_etc
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_useritem`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_useritem`;
CREATE TABLE `tbl_useritem` (
  `FLD_CHARNAME` char(20) NOT NULL,
  `FLD_STDTYPE` char(1) NOT NULL,
  `FLD_MAKEDATE` char(6) NOT NULL,
  `FLD_MAKEINDEX` char(5) NOT NULL,
  `FLD_STDINDEX` int(11) NOT NULL,
  `FLD_DURA` int(11) NOT NULL,
  `FLD_DURAMAX` int(11) NOT NULL,
  `FLD_VALUE1` tinyint(4) NOT NULL,
  `FLD_VALUE2` tinyint(4) NOT NULL,
  `FLD_VALUE3` tinyint(4) NOT NULL,
  `FLD_VALUE4` tinyint(4) NOT NULL,
  `FLD_VALUE5` tinyint(4) NOT NULL,
  `FLD_VALUE6` tinyint(4) NOT NULL,
  `FLD_VALUE7` tinyint(4) NOT NULL,
  `FLD_VALUE8` tinyint(4) NOT NULL,
  `FLD_VALUE9` tinyint(4) NOT NULL,
  `FLD_VALUE10` tinyint(4) NOT NULL,
  `FLD_VALUE11` tinyint(4) NOT NULL,
  `FLD_VALUE12` tinyint(4) NOT NULL,
  `FLD_VALUE13` tinyint(4) NOT NULL,
  `FLD_VALUE14` tinyint(4) NOT NULL,
  PRIMARY KEY (`FLD_MAKEINDEX`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_useritem
-- ----------------------------
